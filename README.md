#**Projekt: Modeliranje pozadine**#

* https://en.wikipedia.org/wiki/Background_subtraction
* https://en.wikipedia.org/wiki/Expectation%E2%80%93maximization_algorithm

##Tehnologije i alati:
- OpenCV: [Mat](http://docs.opencv.org/doc/tutorials/core/mat_the_basic_image_container/mat_the_basic_image_container.html#matthebasicimagecontainer), [OpenCV-Python](http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_core/py_basic_ops/py_basic_ops.html)
- Python 3.5
- C++
- PyCharm 5.0
- Visual Studio 2015



#Faza 1: razvoj postupaka na PC-ju

##a) izvedba glavnog programa 
  - pribavljanje slike iz videa
  - pretvaranje slike u boji u sivu sliku
  - iscrtavanje obrađene slike na ekranu
  - zadavanje parametara
##b) razvoj i vrednovanje jednostavnog postupka nad sivim slikama
  - modelirati pozadinu kao referentnu sivu sliku bez objekata prednjeg plana
  - učiti model sive pozadine na svim slikama slijeda (offline)
    * pretpostavke: objekti su mali, osvjeljenje je konstantno u vremenu
  - varijabilnost svakog piksela p_ij predstaviti histogramom H_ij
    * histogram: polje od 256 cijelih brojeva koje prestavlja distribuciju p_ij u danom videu
      https://en.wikipedia.org/wiki/Histogram
    * elementi histograma: učestalost odgovarajuće sive razine 
  - model pozadine u pikselu p_ij postaviti na mod histograma H_ij
    https://en.wikipedia.org/wiki/Mode_%28statistics%29
  - pribaviti označene skupove podataka
    http://bmc.iut-auvergne.com/?page_id=24
    http://research.microsoft.com/en-us/projects/i2i/data.aspx
    http://research.microsoft.com/en-us/um/people/jckrumm/WallFlower/TestImages.htm
    http://www.vis.uni-stuttgart.de/forschung/informationsvisualisierung-und-visual-analytics/visuelle-analyse-videostroeme/sabs.html
  - vrednovati točnost modela na istom videu na kojem je naučen
    * piksel slike je prednji plan ako se razlikuje od modela za više od x sivih razina
    * x je parametar postupka
##c) razvoj i vrednovanje složenijeg pristupa
   - predstaviti svaki piksel Gaussovom mješavinom s k komponenata (k je parametar)
     https://en.wikipedia.org/wiki/Mixture_model
     * razviti postupak EM
       https://en.wikipedia.org/wiki/Expectation%E2%80%93maximization_algorithm
     * naučiti mješavine na cijelom videu (offline)
     * usporediti vaše rezultate s paketom scikit-learn:
       http://scikit-learn.org/stable/modules/mixture.html
   - vrednovati utjecaj boje i grupiranja piksela u pravokutne superpiksele
   - izvesti inkrementalni postupak učenja i usporediti ga s prethodnim rezultatima
     http://www.ai.mit.edu/projects/vsam/Publications/stauffer_cvpr98_track.pdf

#Faza 2: detekcija uljeza Raspberryjem s kamerom OV5647

##a) razvoj glavnog programa
  - riješiti pribavljanje slike iz kamere
    http://www.pyimagesearch.com/2015/03/30/accessing-the-raspberry-pi-camera-with-opencv-and-python/
  - razviti kod za fonošenje odluke o optimalnom trenutku snimanja
  - riješiti slanje e-maila sa slikom u attachmentu

##b) razvoj postupka za detekciju objekata prednjeg plana
   http://www.ai.mit.edu/projects/vsam/Publications/stauffer_cvpr98_track.pdf

##c) razvoj postupka temeljenog na ugrađenom modulu za detekciju lica
   https://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/robot/face_detection/