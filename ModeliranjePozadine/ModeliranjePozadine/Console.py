﻿from sys import stdin, stdout
import os


class Console:
    _peekchar = None

    class TextStyle:
        HEADER = '\033[95m'
        OKBLUE = '\033[94m'
        OKGREEN = '\033[92m'
        WARNING = '\033[93m'
        FAIL = '\033[91m'
        ENDC = '\033[0m'
        BOLD = '\033[1m'
        UNDERLINE = '\033[4m'

    @staticmethod
    def clear():
        os.system('cls' if os.name == 'nt' else 'clear')
        return ''

    @staticmethod
    def writeline(string="", style=TextStyle.ENDC):
        if style != Console.TextStyle.ENDC:
            string = style + string + Console.TextStyle.ENDC
        stdout.write(string + '\n')

    @staticmethod
    def write(string, style=TextStyle.ENDC):
        if style != Console.TextStyle.ENDC:
            string = style + string + Console.TextStyle.ENDC
        stdout.write(string)

    @staticmethod
    def readline():
        return stdin.readline().rstrip('\n')

    @staticmethod
    def peek():
        return stdin.isatty()
