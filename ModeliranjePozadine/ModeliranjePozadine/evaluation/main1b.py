# coding=utf-8
import cv2
import numpy
from ModeliranjePozadine1.ModeliranjePozadine.background_models import *
import glob
import argparse
import numpy as np
import os

input_path = os.sep+"input"+os.sep+"*"
truth_path = os.sep+"truth"+os.sep+"*"

#Main kao argumente prima:  bgmodel path --threshold --savepath
#bgmodel: 'D' za DistanceBackgroundModel 'I' za IntervalBackgroundModel 'G' za GaussBackgroundModel'
#threshold: iz [0, 1] prag za razlikovanje prednjeg plana od pozadine
#path: Putanja do datoteke koja sadrzi 'input' i 'truth' mape za ucenje i vrednovanje modela
#save_path: Putanja do datoteke za spremanje modela
def main():
    # Postavke za argumente komandne linije
    parser = argparse.ArgumentParser(description='Background model processing')
    parser.add_argument('bgmodel', metavar='BGM',
                        help='\'D\' for DistanceBackgroundModel \n \'I\' for IntervalBackgroundModel \n \'G\' for GaussBackgroundModel')
    parser.add_argument('--threshold', metavar='th', type=float, help='threshold used for generation background model')
    parser.add_argument('path', metavar='path',
                        help='Path to the folder which contains subfolders \'input\' and \'truth\'')

    parser.add_argument('--save_path', metavar='save_path',
                        help='Path where model will be saved.')

    args = parser.parse_args()

    # ucitaj slike
    images = np.array([cv2.imread(img, cv2.IMREAD_GRAYSCALE) for img in glob.glob(args.path + input_path)])
    # postavi model pozadine
    bgmodel = None
    if args.bgmodel == "D":
        if args.threshold is None:
            bgmodel = DistanceBackgroundModel(images[0].shape)
        else:
            bgmodel = DistanceBackgroundModel(images[0].shape, args.threshold)

    elif args.bgmodel == "I":
        if args.threshold is None:
            bgmodel = IntervalBackgroundModel(images[0].shape)
        else:
            bgmodel = IntervalBackgroundModel(images[0].shape, args.threshold)

    elif args.bgmodel == "G":
        if args.threshold is None:
            bgmodel = GaussBackgroundModel(images[0].shape)
        else:
            bgmodel = GaussBackgroundModel(images[0].shape, args.threshold)
    else:
        raise argparse.ArgumentTypeError("Invalid bgmodel passed")

    bgmodel.update(images)
    #spremi model
    if args.save_path is not None:
        bgmodel.save_model(args.save_path)

    # prikazi pozadinu
    cv2.imshow('image', bgmodel.background)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    #ucitaj ocekivane slike prednjeg plana za usporedbu
    truth_images = np.array([cv2.imread(img, cv2.IMREAD_GRAYSCALE) for img in glob.glob(args.path + truth_path)])

    print("Tocnost modela nad nizom slika na kojima je naucen: " + str(compare(bgmodel, images,truth_images ) * 100) + "%")


# Vraca slicnost generiranih slika prednjeg plana danog modela param:bgmodel i istinitih slika prednjeg plana
def compare(bgmodel, input_images, truth_images):
    i = 0
    sim = 0.0
    for input_img in input_images:
        bg_truth_img = bgmodel.detect_foreground(input_img)
        sim += image_similarity(bg_truth_img, truth_images.item(i))
        i += 1
    return sim / truth_images.shape[0]


# Vraca slicnost prve i druge slike
def image_similarity(first_img, second_img):
    bool_array = numpy.isclose(first_img, second_img, 1.e-12)
    return float(numpy.sum(bool_array)) / first_img.size


main()
