﻿from ModeliranjePozadine.ModeliranjePozadine.background_models import *
import cv2
import time
import sys
import numpy as np
import glob

test_name = '311_png'

def main():
    images = np.array([cv2.imread(img, cv2.IMREAD_GRAYSCALE) for img in glob.glob('D:\\MyProjects\\' + test_name +'\\input\\*.png')])
    bm = DistanceBackgroundModel(images[0].shape, 0.95)
    bm.update(images)
	
	#for img in images:
    #    img = cv2.medianBlur(img, 3)
	
    #bm.save_model('D:\\MyProjects\\bm1')
    #bm = IntervalBackgroundModel(images[0].shape, 0.16)
    #bm.load_model('D:\\MyProjects\\IBM_t_999_n_1')
    denoise = False
    while True:
        for img in images:
            dfg = bm.detect_foreground(img, denoise=denoise)
            cv2.imshow('test1', cv2.addWeighted(dfg, 0.6, img, 0.4, 0))

            key = cv2.waitKey(1)
            if key == 27:
                cv2.destroyAllWindows()
                sys.exit(0)
            elif key == ord('s'):
                denoise = ~denoise

main()
