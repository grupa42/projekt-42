﻿from ModeliranjePozadine.ModeliranjePozadine.background_models import *
import cv2
import time
import sys
import numpy as np


def posivi(img):
    return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


def smanji(img, h):
    dim = (h, int(h * img.shape[0] / img.shape[1]))
    return cv2.resize(img, dim, interpolation=cv2.INTER_AREA)


def predobradi(img, smooth):
    img = smanji(posivi(img), 320)
    if smooth:
        img = cv2.medianBlur(img, 7)
    return img


def show_webcam(mirror=False):
    cam = cv2.VideoCapture(0)
    ret_val, img = cam.read()

    denoise = True
    smooth = False

    img = predobradi(img, smooth)

    model_samples_number = 64

    # bm = GaussBackgroundModel(img.shape, 0, buffer_size=model_samples_number)
    bm = IntervalBackgroundModel(img.shape, 0.15, buffer_size=model_samples_number)

    for i in range(500):
        for j in range(model_samples_number):
            ret_val, orig = cam.read()
            img = predobradi(orig, smooth=7)
            bm.push_sample(img)
            time.sleep(0)
            print(repr(j) + "/" + repr(model_samples_number))
        bm.update()
        # cv2.imshow('test', cv2.flip(bm.get_background, 1))
        print("Model pozadine stvoren")
        time.sleep(2)
        while True:
            ret_val, orig = cam.read()
            img = predobradi(orig, smooth=7)
            dfg = bm.detect_foreground(img, denoise=denoise)

            cv2.imshow('test1', cv2.addWeighted(dfg, 0.7, img, 0.3, 0))  # cv2.addWeighted(fg, 0.8, img, 0.2, 0)

            key = cv2.waitKey(1)
            if key == 27:
                cv2.destroyAllWindows()
                sys.exit(0)
            elif key == ord('d'):
                denoise = ~denoise
            elif key == ord('s'):
                smooth = ~smooth


def main():
    show_webcam(mirror=True)


main()
