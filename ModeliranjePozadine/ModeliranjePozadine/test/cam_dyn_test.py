﻿import sys
import cv2
import numpy as np
from ModeliranjePozadine.ModeliranjePozadine.background_models import *
import os

def posivi(img):
    return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


def smanji(img):
    dim = (400, int(400 * img.shape[0] / img.shape[1]))
    return cv2.resize(img, dim, interpolation=cv2.INTER_AREA)


def debackgroundize(image, mask):
    new = cv2.bitwise_and(image, image, mask=mask)
    return new


def show_webcam(mirror=False):
    cam = cv2.VideoCapture(0)
    ret_val, img = cam.read()

    img = smanji(posivi(img))

    dbm = DistanceBackgroundModelD(img.shape, 0.15, buffer_size=128)

    for i in range(500):
        denoise = False
        while True:
            ret_val, orig = cam.read()
            orig = smanji(orig)
            img = posivi(orig)
            fg = dbm.detect_foreground(img, denoise=denoise)
            # rects = drawrectangle(dfg)
            # cv2.imshow('test1', cv2.addWeighted(rect, 0.7, img, 0.3, 0))  # cv2.addWeighted(fg, 0.8, img, 0.2, 0)
            # cv2.imshow('test1', cv2.addWeighted(cv2.Canny(fg, 128, 128), 0.7, img, 0.3, 0))  # cv2.addWeighted(fg, 0.8, img, 0.2, 0)
            cv2.imshow('test1', debackgroundize(orig, fg))  # cv2.addWeighted(fg, 0.8, img, 0.2, 0)

            if i % 1 == 0:
                dbm.push_sample_update(img)

            key = cv2.waitKey(1)
            if key == 27:
                cv2.destroyAllWindows()
                sys.exit(0)
            elif key == ord('S'):
                denoise = ~denoise


def main():
    show_webcam(mirror=True)

print(os.path.dirname(os.path.realpath(__file__)))
main()
