﻿from ModeliranjePozadine.ModeliranjePozadine.background_models import *
import cv2
import time
import sys
import numpy as np
import glob


def main():
    images = np.array(
        [cv2.imread(img, cv2.IMREAD_GRAYSCALE) for img in glob.glob('D:\\MyProjects\\111_png\\input\\*.png')])
    print("Images loaded.")

    t = time.time()
    bm = DistanceBackgroundModel(images[0].shape, 0.16)
    bm.threshold = 0.2
    bm.update(images)
    print("Background model created in " + repr(int(1000*(time.time() - t))) + " ms")

    bm.save_model('D:\\MyProjects\\111_t_' + repr(bm.threshold))
    print("Background model saved.")

main()
