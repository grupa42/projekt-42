__author__ = 'Bulic'
import numpy as np
import cv2
import sys

# Objekt VideoCapture kao argument prima index kamere (u ovom slucaju 0 - integrirana) ili apsolutnu putanju do videa
cap = cv2.VideoCapture(0)

while(True):
    # Uzima sliku po sliku
    ret, frame = cap.read()

    # Pretvaranje slike u boji u sivu sliku
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Prikaz slike na ekranu
    cv2.imshow('video',gray)

    # Spremanje zeljene slike za kasniju repordukciju
    if cv2.waitKey(1) & 0xFF == ord('c'):
        temp = gray

    # Uvijet izlaska
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break


# Unistavanje prozora s videom, prikaz spremljene slike
cv2.destroyWindow('video')
cv2.imshow('frame',temp)
cv2.waitKey(0)

# Zatvaranje stvorenih prozora
cap.release()
cv2.destroyAllWindows()


