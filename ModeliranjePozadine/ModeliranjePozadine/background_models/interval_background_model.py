﻿# coding=utf-8
from background_model import *
import time


class IntervalBackgroundModel(BackgroundModel):
    # Inicijalizira i odredjuje maksimalni broj slika koje objekt moze drzati
    threshold_default = 0.98

    @staticmethod
    def is_outside(number, interval):
        return number < interval[0] or number > interval[1]

    def __init__(self, image_size, threshold=threshold_default, buffer_size=0):
        BackgroundModel.__init__(self, image_size, threshold, buffer_size)
        self._bg_ranges = np.zeros((self.image_size[0], self.image_size[1]), dtype=tuple)
        self._is_outside = np.vectorize(self.is_outside)

    ## Prepoznaje objekte prednjeg plana
    def detect_foreground(self, image, denoise=False):
        foreground_image = np.zeros(self.image_size, np.uint8)
        foreground_image[self._is_outside(image, self._bg_ranges)] = 255
        return self._filter_foreground_noise(foreground_image) if denoise else foreground_image

    # Izradjuje model pozadine na temelju zadnjih <self._buffer.count> dodanih slika
    def update(self, images=None):
        if images is None:
            images = np.array([img for img in self._buffer])
        self._create_histograms(images)
        # self._smooth_histograms(1, 0.5)
        np.argmax(self._histograms, axis=2, out=self._background)
        self._calculate_background_ranges(self._histograms, len(images))

    def _calculate_background_ranges(self, histograms, sample_count):
        bgranges = np.empty_like(self._bg_ranges)
        if self._threshold >= 1:
            for i in range(self.image_size[0]):
                for j in range(self.image_size[1]):
                    bgranges.itemset((i, j), (0, 255))
            return

        threshold_sum = int(self._threshold * sample_count)
        for i in range(self.image_size[0]):
            for j in range(self.image_size[1]):
                start = end = self._background.item(i, j)
                binsum = histograms.item(i, j, start)
                start -= 1
                end += 1
                leftbin = histograms.item(i, j, start)
                rightbin = histograms.item(i, j, end)
                lastright = False
                while binsum < threshold_sum:
                    if leftbin > rightbin or (leftbin == rightbin and lastright):
                        binsum += leftbin
                        start -= 1
                        leftbin = histograms.item(i, j, start) if start >= 0 else -1
                        lastright = False
                    else:
                        binsum += rightbin
                        end += 1
                        rightbin = histograms.item(i, j, end) if end <= 255 else -1
                        lastright = True
                bgranges.itemset((i, j), (start + 1, end - 1))
        self._bg_ranges = bgranges


    def save_model(self, path):
        data = np.asarray((self.image_size, self._background, self._histograms, self._threshold, self._bg_ranges))
        np.ndarray.dump(data, open(path, 'wb'))

    def load_model(self, path):
        self.image_size, self._background, self._histograms, self._threshold, self._bg_ranges = np.load(
                open(path, 'rb'))
