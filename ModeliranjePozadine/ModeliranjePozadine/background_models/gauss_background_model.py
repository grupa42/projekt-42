import numpy as np
from background_model import *
import scipy
import scipy.stats as stats
from matplotlib import pyplot as plt
import pylab as pl


class GaussBackgroundModel(BackgroundModel):
    threshold_default = 0.85

    def __init__(self, imageSize, threshold=threshold_default, buffer_size=0, tolerance=0.001, k=1):
        self.images = None
        self.imageSize = imageSize
        self.param = threshold
        self.tolerance = tolerance
        self.k = k
        self._normalDistributions = None
        BackgroundModel.__init__(self, imageSize, threshold, buffer_size)

        self.normalMean = np.empty([self.imageSize[0], self.imageSize[1], self.k])
        self.normalVariance = np.empty([self.imageSize[0], self.imageSize[1], self.k])
        self.normalWeight = np.empty([self.imageSize[0], self.imageSize[1], self.k])

    def plotPixelDistribution(self, x, y):
        mean = self.normalMean[x, y, :]
        variance = self.normalVariance[x, y, :]

        variance[variance == 0] = self.tolerance

        X = self.images[:, x, y]
        A = list(X)
        A.sort()
        pdf = np.zeros(self.k)
        for i in range(self.k):
            pdf = stats.norm.pdf(A, np.asarray(mean[i]), np.asarray(variance[i]))
            pl.plot(A, pdf, '-o')
        pl.hist(A, normed=True)
        pl.show()

    def update(self, images=None):
        if images is None:
            images = np.array([img for img in self._buffer])
        self.images = images
        self._normalDistributions = self._gaussMixtureWithEM(self.k)

    def detect_foreground(self, image, denoise=False):
        var = self.normalVariance[:, :, 0]
        mu = self.normalMean[:, :, 0]
        var[var < self.tolerance] = self.tolerance
        G = np.zeros(shape=mu.shape, dtype=np.uint8)
        G[np.abs(image - mu) / var ** 0.5 > 2.807] = 255
        G[var < self.tolerance] = 0
        G[np.abs(image - mu) <= self.tolerance] = 0

        tempW = np.copy(np.ones(image.shape) - self.normalWeight[:, :, 0])
        for i in range(self.k - 1):
            w = self.normalWeight[:, :, i + 1]
            var = self.normalVariance[:, :, i + 1]
            mu = self.normalMean[:, :, i + 1]
            var[var < self.tolerance] = self.tolerance
            G[(tempW >= 1 - self.param) & (np.abs(image - mu) / var ** 0.5 > 0.5)] = 255
            tempW = tempW - w

        return self._filter_foreground_noise(G) if denoise else G

    def _resetData(self):
        self.data = np.zeros((self.imageSize[0], self.imageSize[1], len(self.images)), dtype=np.uint32)

    def _gaussMixtureWithEM(self, k):

        number, width, height = self.images.shape[0], self.images.shape[1], self.images.shape[2]
        Y = np.reshape(self.images.T, (self.images.shape[1] * self.images.shape[2], self.images.shape[0]))

        if k == 1:
            for j in range(height):
                for i in range(width):
                    X = Y[j * width + i]
                    a = np.asarray([1.0])
                    b = np.asarray([0.0] * (k - 1))
                    self.normalWeight[i][j] = np.concatenate((a, b))
                    a = np.asarray([X.mean(axis=0)])
                    b = np.asarray([0.0] * (k - 1))
                    self.normalMean[i][j] = np.concatenate((a, b))
                    a = np.asarray([X.var(axis=0)])
                    b = np.asarray([0.0] * (k - 1))
                    self.normalVariance[i][j] = np.concatenate((a, b))
            return

        for j in range(height):
            for i in range(width):
                X = Y[j * width + i]

                w = np.asarray([1. / k] * k)
                a = np.asarray([X.mean(axis=0)])
                b = np.asarray([0.0] * (k - 1))
                mu = np.concatenate((a, b))
                a = np.asarray([X.var(axis=0)])
                b = np.asarray([X.var(axis=0) / 2.] * (k - 1))
                var = np.concatenate((a, b))
                logLikelihoods = []

                if X.var(axis=0) > self.tolerance:

                    while (1):

                        stack = np.asarray([X] * k)
                        numerator = w * stats.norm.pdf(stack.T, loc=mu, scale=var ** 0.5)
                        denominator = np.reshape(np.sum(numerator, axis=1), (number, 1))
                        responsibilities = numerator / denominator

                        finalSum = np.sum(responsibilities * np.reshape(X, (number, 1)), axis=0)
                        Sum = np.sum(responsibilities, axis=0)
                        mu2 = finalSum / Sum
                        # mu2[np.isnan(mu2) == True] = 0.0
                        var2 = np.sum((stack.T - mu2) ** 2 * responsibilities, axis=0) / Sum
                        # var2[np.isnan(var2) == True] = 0.001
                        var2[var2 < 0.001] = 0.001
                        w2 = np.sum(responsibilities, axis=0) / number

                        delta = np.asarray([mu - mu2, var - var2, w - w2])

                        mu = np.copy(mu2)
                        var = np.copy(var2)
                        w = np.copy(w2)

                        first = w * stats.norm.pdf(stack.T, loc=mu, scale=var ** 0.5)
                        second = np.reshape(np.sum(first, axis=1), (number, 1))
                        logLikelihood = np.sum(np.log(second))
                        if (np.isnan(logLikelihood) == True):
                            logLikelihood = 0.0

                        logLikelihoods.append(logLikelihood)
                        if len(logLikelihoods) < 2: continue

                        if (((np.max(np.abs(delta[2])) < self.tolerance) or (np.max(np.abs(delta[0])) <
                                                                                 self.tolerance) or (
                                    np.min(var) <= self.tolerance)) or
                                (np.abs(logLikelihoods[-1] - logLikelihoods[-2]) < self.tolerance)):
                            self.normalWeight[i][j] = np.copy([w])
                            self.normalMean[i][j] = np.copy([mu])
                            self.normalVariance[i][j] = np.copy([var])
                            break

                else:
                    a = np.asarray([1.0])
                    b = np.asarray([0.0] * (k - 1))
                    self.normalWeight[i][j] = np.concatenate((a, b))
                    a = np.asarray([X.mean(axis=0)])
                    b = np.asarray([0.0] * (k - 1))
                    self.normalMean[i][j] = np.concatenate((a, b))
                    a = np.asarray([X.var(axis=0)])
                    b = np.asarray([0.0] * (k - 1))
                    self.normalVariance[i][j] = np.concatenate((a, b))

        return

    def save_model(self, path):
        data = np.asarray((self.image_size, self.tolerance, self.param, self.k, self.normalMean, self.normalVariance,
                           self.normalWeight))
        np.ndarray.dump(data, open(path, 'wb'))

    def load_model(self, path):
        self.image_size, self.tolerance, self.param, self.k, self.normalMean, self.normalVariance, self.normalWeight = np.load(
            open(path, 'rb'))
