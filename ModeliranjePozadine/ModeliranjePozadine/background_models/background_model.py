﻿# coding=utf-8
import numpy as np
from limited_set import LimitedSet
from abc import ABCMeta
import cv2


# Model pozadine
class BackgroundModel:
    __metaclass__ = ABCMeta

    # Inicijalizira i odredjuje maksimalni broj slika koje objekt moze drzati
    def __init__(self, image_size, threshold=0.2, buffer_size=0):
        """
        :param image_size: dimenzije pozadine (širina, visina)
        :param threshold: iz [0, 1] prag za razlikovanje prednjeg plana od pozadine
        :param buffer_size: broj slika koje se koriste kod dinamičnog modela pozadine
        """
        self.image_size = image_size  # dimenzije pozadine: (visina, širina)
        self._background = np.empty(self.image_size, dtype=np.uint8)  # pozadina
        self._threshold = threshold
        self._buffer = LimitedSet(buffer_size)
        self._histograms = None
        self._median_ksize = 7
        self._histograms = np.zeros((self.image_size[0], self.image_size[1], 256), dtype=np.uint16)


    @property
    def background(self):
        return self._background

    @property
    def buffer_size(self):
        return len(self._buffer)

    @property
    def threshold(self):
        return self._threshold

    @threshold.setter
    def threshold(self, threshold):
        self._threshold = threshold

    @property
    def median_ksize(self):
        return self._median_ksize

    @threshold.setter
    def median_ksize(self, median_ksize):
        if median_ksize % 2 == 0:
            median_ksize -= 1
        if median_ksize < 0:
            median_ksize = 1
        self._median_ksize = median_ksize


        # Dodaje sliku u skup slika za modeliranje pozadine

    def push_sample(self, image):
        self._buffer.add(image)

    # Izradjuje model pozadine na temelju zadnjih n dodanih slika
    def update(self, images=None):
        raise NotImplemented

    def _create_histograms(self, images):
        histograms = np.zeros((self.image_size[0], self.image_size[1], 256), dtype=np.uint16)
        for i in range(self.image_size[0]):
            for j in range(self.image_size[1]):
                histograms[i, j] = np.bincount(images[:, i, j], minlength=256)
        self._histograms = histograms

    def _smooth_histograms(self, n_times, factor):
        factor /= 2
        old_histograms = np.copy(self._histograms)
        for i in range(self._histograms.shape[0]):
            for j in range(self._histograms.shape[1]):
                for k in range(255):
                    change = int(factor * (old_histograms.item(i, j, k + 1) - old_histograms.item(i, j, k)))
                    np.add.at(self._histograms, (i, j, k), change)
                    np.add.at(self._histograms, (i, j, k + 1), -change)
            print(repr(i) + "/" + repr(self._histograms.shape[0]))

    # Prepoznaje objekte prednjeg plana
    def detect_foreground(self, image, denoise=False):
        raise NotImplemented

    # uzima najčešću boju u okruženju svakog piksela
    def _filter_foreground_noise(self, fg):
        if self._median_ksize > 1:
            return cv2.medianBlur(fg, self._median_ksize)
        return fg

    def save_model(self, path):
        raise NotImplemented

    def load_model(self, path):
        raise NotImplemented
