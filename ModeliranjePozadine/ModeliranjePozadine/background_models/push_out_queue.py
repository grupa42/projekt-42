# coding=utf-8
import numpy as np


# Tip podataka koji čuva zadnjih n dodanih elemenata a starije briše
class PushOutQueue:
    # Inicijalizira i određuje maksimalan broj elemenata koje može držati
    def __init__(self, max_count):
        self._size = max_count
        self._data = np.empty(max_count, dtype=np.ndarray)
        self._count = 0
        self._last = -1

    @property
    def size(self):
        return self._size

    @property
    def count(self):
        return self._count

    @property
    def _first(self):
        i = self._count + self._last
        return i if i < self._size else i - self._size

    def _incr(self, n):
        n += 1
        return n if n < self._size else n - self._size

    def _decr(self, n):
        n -= 1
        return n if n >= 0 else n + self._size

    # Iterator za for petlju
    def __iter__(self):
        i = self._last
        for k in range(self._count):
            i = self._decr(i)
            yield self._data.item(i)

    def __getitem__(self, item):
        return self._data.item(item)

    def __len__(self):
        return self._count

        # Dodaje novi element i vraća najstariji ako broj elemenata prijeđe max_count, inače None.

    def push(self, element):
        self._last = (self._last + 1) if self._last != self.size - 1 else 0
        if self._count != self.size:
            self._count += 1
        ret = None if self._count == 0 else self._data[self._last]
        self._data[self._last] = element
        return ret

    # Uklanja sve elemente
    def clear(self):
        self._count = 0
        self._last = -1
