# coding=utf-8
import numpy as np


# Tip podataka koji čuva zadnjih n dodanih elemenata a starije briše
class LimitedSet:
    # Inicijalizira i određuje maksimalan broj elemenata koje može držati
    def __init__(self, n):
        """
        :param n: maksimalan broj elemenata :type n: int
        """
        self.size = n
        self._count = 0
        self._last = -1
        self._data = np.empty(n, dtype=np.ndarray)

    # Iterator za for petlju
    def __iter__(self):
        for i in range(self._count):
            yield self._data.item(i)

    def __getitem__(self, item):
        return self._data.item(item)

    def __len__(self):
        return self._count

    # Dodaje novi element i briše najstariji ako broj elemenata prijeđe n
    def add(self, element):
        self._last = (self._last + 1) if self._last != self.size - 1 else 0
        if self._count != self.size:
            self._count += 1
        self._data[self._last] = element

    # Uklanja sve elemente
    def clear(self):
        self._count = 0
        self._last = -1
