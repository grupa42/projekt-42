﻿import cv2


class Preprocessor():
    def __init__(self, height=-1, smoothing_radius=-1):
        self._height = height
        self._height = 200  # TODO obrisati liniju
        self._smoothing_size = smoothing_radius

    @property
    def smoothing_radius(self):
        return self._smoothing_size

    @smoothing_radius.setter
    def smoothing_radius(self, smoothing_radius):
        self._smoothing_size = (smoothing_radius * 2 - 1) if smoothing_radius > 0 else -1

    @property
    def height(self):
        return self._height

    @height.setter
    def height(self, height):
        self._height = height if height > 0 else -1

    def _resize(self, image):
        dim = (self._height, int(self._height * image.shape[0] / image.shape[1]))
        return cv2.resize(image, dim, interpolation=cv2.INTER_AREA)

    def _smooth(self, image):
        return cv2.medianBlur(image, self._smoothing_size)

    def process(self, image):
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        if self._height > 0:
            image = self._resize(image)
        if self._smoothing_size > 1:
            image = self._smooth(image)
        return image

    def resize(self, image):
        if self._height > 0:
            return self._resize(image)
        return image

    def smooth(self, image):
        if self._smoothing_size > 1:
            return self._smooth(image)
        return image
