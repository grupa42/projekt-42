__author__ = 'Bolic'

import smtplib
import os


# Email paketi potrebnih modula
# MIME - Multipurpose Internet Mail Extension (ekstenzija SMTP)
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart

COMMASPACE = ', '

# ~detekcija - putanja u kojoj se nalazi slika
# from file import emailImage(detekcija)
# !!!POTREBNE IZMJENE U FUNKCIJI: SENEDER, RECIEVER, PASSWORD

def emailImage (detekcija, recipient):

    # ~me (potrebno ispuniti) = email adresa posiljatelja - mora biti u obliku @gmail
    # ~to (potrebno ispuniti) = lista ljudi kojima se salje email (razdvojeni "," : COMMASPACE) -> mjihove email adrese
    # argumenti za join se zadaju u obliku .join("a", "b", "c", ...) -> msg['To'] = COMMASPACE.join(to)
    # ako saljemo samo jednom primatelju, direktno upisati email istog (inace koristiti ovo gore)

    me = "projekt.grupa42@gmail.com"
    to = recipient

    # Sadrzaj email-a u nastavku
    msg = MIMEMultipart()
    msg["Subject"] = 'Detektiran uljez!'
    msg['From'] = me
    msg['To'] = to
    # ~preamble - The format of a MIME document allows for some text between the blank line following the headers, and the first multipart boundary string.
    # Normally, this text is never visible in a MIME-aware mail reader because it falls outside the standard MIME armor
    # However, when viewing the raw text of the message, or when viewing the message in a non-MIME aware reader, this text can become visible.
    msg.preamble = 'Face detected!'

    fp = open(detekcija, 'rb')
    img = MIMEImage(fp.read())
    fp.close()
    msg.attach(img)


    # Send the email via SMTP server
    # ~PASSWORD - sifra xyz@gmail.com adrese
    s = smtplib.SMTP('smtp.gmail.com:587')
    s.starttls()
    s.set_debuglevel(False)
    s.login(me, "grupa42projekt")
    s.sendmail(me, to, msg.as_string())
    s.quit()

