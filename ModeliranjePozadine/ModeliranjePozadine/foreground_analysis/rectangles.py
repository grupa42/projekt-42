import cv2

def drawrectangle(im):
    def getpoints(contours):
        points = []
        for contour in contours:
            i = 0
            while i < len(contour):
                x = contour[i][0][0]
                y = contour[i][0][1]
                point = (x, y)
                points.append(point)
                i += 1
        return points

    def findobjects(allPoints):
        counter = 0

        # No objects (black picture)
        if len(allPoints) < 1:
            return -1

        current_list = [allPoints[counter]]
        sve = []

        while counter < (len(allPoints) - 1):
            if len(current_list) == 0:
                current_list.append(allPoints[counter])
            x1 = allPoints[counter][0]
            y1 = allPoints[counter][1]
            x2 = allPoints[counter + 1][0]
            y2 = allPoints[counter + 1][1]

            diffX = abs(x2 - x1)
            diffY = abs(y2 - y1)

            # Define the length of scope to search
            if (diffX <= 15) & (diffY <= 15):
                current_list.append(allPoints[counter + 1])
                counter += 1
                if counter == (len(allPoints) - 1):
                    sve.append(current_list)
            else:
                # Object found
                sve.append(current_list)
                current_list = []
                counter += 2
        return sve

    def findmax(camobjectlist):
        max_x = camobjectlist[0][0]
        max_y = camobjectlist[0][1]
        min_x = max_x
        min_y = max_y
        for cameraobject in camobjectlist:
            if cameraobject[0] > max_x:
                max_x = cameraobject[0]
            if cameraobject[0] < min_x:
                min_x = cameraobject[0]

            if cameraobject[1] > max_y:
                max_y = cameraobject[1]
            if cameraobject[1] < min_y:
                min_y = cameraobject[1]
        lista = (max_x, min_x, max_y, min_y)
        return lista

    def getmaxpointlist(sve):
        provjera = []
        for cameraObject in sve:
            maxMin = findmax(cameraObject)
            point1 = (maxMin[1], maxMin[2])
            point2 = (maxMin[0], maxMin[3])
            points = (point1, point2)
            provjera.append(points)
        return provjera

    def optimize(provjera, im):
        old_maximum_points = []
        maxObject = provjera[0]
        old_max_object = ((0, 0), (0, 0))
        while 1:
            zbroj = 0
            # pronadji novi max
            for points in provjera:
                if points not in old_maximum_points:
                    x1 = points[0][0]
                    y1 = points[0][1]
                    x2 = points[1][0]
                    y2 = points[1][1]
                    zbroj2 = abs(x2 - x1) + abs(y2 - y1)
                    if zbroj2 > zbroj:
                        maxObject = points
                        zbroj = zbroj2

            # remove default max
            provjera.remove(maxObject)

            while 1:
                # save the current maxObject
                maxObjectOld = maxObject
                for points in provjera:
                    x1 = points[0][0]
                    y1 = points[0][1]
                    x2 = points[1][0]
                    y2 = points[1][1]

                    if (x1 + 1 >= maxObject[0][0]) & (x1 - 1 <= maxObject[1][0]) & (y1 - 1 <= maxObject[0][1]) & (
                                    y1 + 1 >= maxObject[1][1]):
                        if not ((x2 + 1 > maxObject[0][0]) & (x2 - 1 < maxObject[1][0]) & (y2 - 1 < maxObject[0][1]) & (
                                        y2 + 1 > maxObject[1][1])):
                            provjera.remove(points)
                            if x2 > maxObject[1][0]:
                                point = (x2 + 5, maxObject[1][1] - 5)
                                maxObject = (maxObject[0], point)
                            if y2 < maxObject[1][1]:
                                point = (maxObject[1][0] + 5, y2 - 5)
                                maxObject = (maxObject[0], point)
                    if (x2 + 1 >= maxObject[0][0]) & (x2 - 1 <= maxObject[1][0]) & (y2 - 1 <= maxObject[0][1]) & (
                                    y2 + 1 >= maxObject[1][1]):
                        if not ((x1 > maxObject[0][0]) & (x1 < maxObject[1][0]) & (y1 < maxObject[0][1]) & (
                                    y1 > maxObject[1][1])):
                            if points in provjera:
                                provjera.remove(points)
                            if x1 < maxObject[0][0]:
                                point = (x1 - 5, maxObject[0][1] + 5)
                                maxObject = (point, maxObject[1])
                            if y1 > maxObject[0][1]:
                                point = (maxObject[0][0] - 5, y1 + 5)
                                maxObject = (point, maxObject[1])
                # no more objects
                if maxObjectOld == maxObject:
                    break

            # list to remove objects from - optimized list
            provjera2 = []
            for p in provjera:
                provjera2.append(p)

            # remove objects inside bigger objects
            for points in provjera:
                x1 = points[0][0]
                y1 = points[0][1]
                x2 = points[1][0]
                y2 = points[1][1]

                # check if point is inside
                if (x1 >= maxObject[0][0]) & (x1 <= maxObject[1][0]) & (y1 <= maxObject[0][1]) & (
                            y1 >= maxObject[1][1]):
                    if (x2 >= maxObject[0][0]) & (x2 <= maxObject[1][0]) & (y2 <= maxObject[0][1]) & (
                                y2 >= maxObject[1][1]):
                        # delete inside point
                        provjera2.remove(points)

            # add the new max
            provjera2.append(maxObject)

            old_maximum_points.append(maxObject)

            # change the list to the optimized one
            provjera = provjera2

            # check if last object
            if old_max_object == maxObject:
                for cameraObject in provjera:
                    # show new picture
                    cv2.rectangle(im, cameraObject[0], cameraObject[1], (255, 0, 0), 2)
                return im

            old_max_object = maxObject

    try:
        _, contours, _ = cv2.findContours(im, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    except:
        contours, _ = cv2.findContours(im, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    allPoints = getpoints(contours)

    if len(allPoints) != 0:
        all = findobjects(allPoints)

        # If no objects return -1
        if all == -1:
            return None

        provjera = getmaxpointlist(all)
        picture = optimize(provjera, im)
        return picture
    else:
        return None
