﻿# -*- coding: Utf-8 -*-
import random
import sys
from background_models import *
import cv2
from console import Console
from camera import Camera
import time
from preprocessor import Preprocessor
from foreground_analysis import *
from face_detection import *
from mail import emailImage
import numpy as np
import threading2 as threading

KEY_ESC = 27
KEY_UP = 2490368
KEY_DOWN = 2621440
KEY_LEFT = 2424832
KEY_RIGHT = 2555904


class IntruderDetector:
    def __init__(self, buffer_size=32):
        self._cam = cv2.VideoCapture(0)

        self._bm = None
        self._bm_buffer_size = buffer_size
        self._gauss_k = 1
        self._denoise = True
        self._medianblur_ksize = 7

        self._preprocessor = Preprocessor(height=-1)

        self._min_fg_ratio = 0.05  # minimalni omjer piksela prednjeg plana

        self._detection_interval = 0.1  # << _update_interval interval traženja uljeza /s
        self._updating = False
        self._push_interval = 0.5  # iz [0,1], 1 = potpuna zamjena sadržaja buffera prije ažuriranja

        self._email_enabled = False
        self._email = "projekt.grupa42@gmail.com"

        self._display = True

    @staticmethod
    def debackgroundize(image, mask):
        new = cv2.bitwise_and(image, image, mask=mask)
        return new

    # w = None
    def get_foreground_ratio(self, foreground_mask):
        f = np.count_nonzero(foreground_mask)
        return float(f) / (foreground_mask.shape[0] * foreground_mask.shape[1])

    def get_image_from_camera(self):
        return Camera.get_image()

    def _setup_bm(self, bm=DistanceBackgroundModel):
        confirmed = False
        thresh = bm.threshold_default
        while not confirmed:
            Console.writeline("\nPostavljanje parametara", style=Console.TextStyle.UNDERLINE)
            print("'r' povratak")
            print("'c' potvrda i nastavak")
            print(
                "'thr <broj>' prag = broj iz [0,1], (={0})".format(repr(thresh)))
            print("'buf <broj>' veličina buffera = broj, (={0})".format(repr(self._bm_buffer_size)))
            print("'med <broj>' dimenzija medijana, (={0})".format(repr(self._medianblur_ksize)))
            if bm == GaussBackgroundModel:
                print("'k <broj>' broj komponenata, (={0})".format(repr(self._gauss_k)))

            Console.write("> ")
            cmd = Console.readline().lower().split(' ')
            if cmd[0] == "r":
                return False
            elif cmd[0] == "c":
                break
            elif cmd[0] == "thr":
                thresh = float(cmd[1])
                if thresh < 0 or thresh > 1:
                    thresh = -1
            elif cmd[0] == "buf":
                buf = int(cmd[1])
                if buf > 0:
                    self._bm_buffer_size = buf
            elif cmd[0] == "med":
                m = int(cmd[1])
                if m % 2 == 0:
                    m -= 1
                if m > 0:
                    self._medianblur_ksize = m
            elif cmd[0] == "k":
                k = int(cmd[1])
                if k > 0:
                    self._gauss_k = k

        img = self.get_image_from_camera()
        img = self._preprocessor.process(img)
        self._bm = bm(img.shape, threshold=thresh, buffer_size=self._bm_buffer_size)
        self._bm._median_ksize = self._medianblur_ksize
        if bm == GaussBackgroundModel:
            self._bm.k = self._gauss_k
        return True

    def _choose_bm(self):
        selected = False
        while not selected:
            Console.writeline("\nOdabir vrste modela pozadine", style=Console.TextStyle.UNDERLINE)
            print("'exit'|'izlaz' izlaz")
            print("'d' DistanceBackgroundModel")
            print("'i' IntervalBackgroundModel")
            print("'g' GaussBackgroundModel")
            print("'t' DistanceBackgroundModelDTest")
            Console.write("> ")
            selected = False
            cmd = Console.readline().lower()
            print(cmd)
            if cmd == "exit" or cmd == "izlaz":
                sys.exit()
            elif cmd == "d":
                selected = self._setup_bm(DistanceBackgroundModel)
            elif cmd == "i":
                selected = self._setup_bm(IntervalBackgroundModel)
            elif cmd == "g":
                selected = self._setup_bm(GaussBackgroundModel)
            elif cmd == "t":
                print("Pokrenite '/test/cam_dyn_test.py'.")
                sys.exit()

    def accept_commands(self):
        pass

    def _fill_buffer(self, n=-1):
        if n < 0:
            n = self._bm_buffer_size
        for i in range(n):
            img = self.get_image_from_camera()
            img = self._preprocessor.process(img)
            self._bm.push_sample(img)
            Console.write("\r")
            Console.write("{0}/{1}".format(i + 1, n))
        print("")

    def assign_work_to_new_thread(self, work, limit):
        t = threading.Thread(target=work)
        t.priority = 0.1
        t.start()

    def _run(self):
        close = False
        while not close:
            Console.writeline("\nInicijalizacija modela pozadine", style=Console.TextStyle.UNDERLINE)
            self._fill_buffer()
            Console.writeline("\nInicijalno računanje modela pozadine", style=Console.TextStyle.UNDERLINE)
            self._bm.update()

            def start_updater(self):
                def updater(self):
                    while not close:
                        self._bm.update()
                        if not self._updating:
                            time.sleep(0.1)

                t = threading.Thread(target=lambda: updater(self))
                t.priority = 0.1
                t.start()

            start_updater(self)

            tlast = 0.0
            tpush = 0.0
            emailinterval = 1
            emailintervalmin = 1
            temail = 0

            while not close:
                Console.writeline("\nČekanje uljeza", style=Console.TextStyle.UNDERLINE)
                print("[ESC] zatvaranje programa")
                print("[r] povratak u izbornik")
                print("[n] ponovna inicijalizacija")
                print("[d] uključivanje/isključivanje prikaza")
                print("[u] uključivanje/isključivanje prilagođavanja promjenama pozadine")
                print("[e] uključivanje/isključivanje prepoznavnja lica i slanja e-mailom")
                print("[+]/[-] povećavanje/smanjivanje praga")
                print("[s] uključivanje/isključivanje uglađivanja")
                print("[*]/[/] povećavanje/smanjivanje uglađivanja")
                print("[CTRL]+[K] spremanje postavki u datoteku")
                print("[CTRL]+[L] učitavanje spremljenih postavki iz datoteke")
                print(" Prilagođavanje: " + ("uključeno" if self._updating else "isključeno"))
                print(" E-mail: " + ("uključeno" if self._email_enabled else "isključeno"))
                while True:
                    tnow = time.time()
                    dt = tnow - tlast
                    face = None
                    if dt > self._detection_interval:
                        tlast = tnow
                        origimg = self.get_image_from_camera()
                        grayimg = self._preprocessor.process(origimg)
                        fgmask = self._bm.detect_foreground(grayimg, denoise=self._denoise)

                        fgratio = self.get_foreground_ratio(fgmask)
                        Console.write("\r *Foreground ratio:{0:.2f}".format(fgratio))

                        if fgratio == self._min_fg_ratio / 4:
                            emailinterval = emailintervalmin

                        if self._email_enabled:
                            dt = tnow - temail
                            if dt > emailinterval:
                                rectangles = drawrectangle(fgmask) if False else []
                                if rectangles is not None:
                                    face = face_detection.detect_face(origimg)
                                    if face is not None:
                                        t = threading.Thread(target=lambda: emailImage("detekcija.jpg", self._email))
                                        t.start()
                                        temail = tnow
                                        emailinterval *= 2
                                        print(repr(emailinterval))
                                        Console.writeline("\rE-mail: poslana slika uljeza")
                                        cv2.imshow('Čekanje uljeza', face)
                                        break

                        if self._updating:
                            dt = tnow - tpush
                            if dt > self._push_interval:
                                t = threading.Thread(target=lambda: self._bm.push_sample(grayimg))
                                t.start()
                                tpush = tnow
                                # self._bm.push_sample(grayimg)

                    if self._display:
                        if face is not None:
                            cv2.imshow('Čekanje uljeza', face)
                        else:
                            cv2.imshow('Čekanje uljeza', cv2.addWeighted(fgmask, 0.7, grayimg, 0.3, 0))

                    key = cv2.waitKey(1) % 256
                    if key == ord('d'):
                        cv2.destroyAllWindows()
                        self._display = not self._display
                    if not self._display:
                        cv2.imshow('Čekanje uljeza', np.zeros([1, grayimg.shape[1]]))
                    elif key == ord('s'):
                        self._denoise = not self._denoise
                    elif key == ord('u'):
                        self._updating = not self._updating
                        if self._updating:
                            start_updater(self)
                        break
                    elif key == ord('e'):
                        self._email_enabled = not self._email_enabled
                        break
                    elif key == ord('n'):
                        break
                    elif key == ord('r'):
                        return 0
                    elif key == KEY_ESC:  # ESC
                        cv2.destroyAllWindows()
                        close = True
                        break
                    elif key == ord('+'):
                        self._bm.threshold += 0.02
                        if self._bm.threshold > 1:
                            self._bm.threshold = 1
                    elif key == ord('-'):
                        self._bm.threshold -= 0.02
                        if self._bm.threshold < 0:
                            self._bm.threshold = 0
                    elif key == ord('*'):
                        self._medianblur_ksize += 2
                        self._bm.median_ksize = self._medianblur_ksize
                    elif key == ord('/'):
                        self._medianblur_ksize -= 2
                        if self._medianblur_ksize < 0:
                            self._medianblur_ksize = 0
                        self._bm.median_ksize = self._medianblur_ksize
        return 1 if close else 0

    def run(self):
        Console.writeline("\nDetektor uljeza", style=Console.TextStyle.UNDERLINE)
        Console.write("Adresa e-pošte primatelja: ")
        line = Console.readline()
        if len(line) > 0:
            self._email = line
        self._choose_bm()
        while self._run() == 0:
            self._choose_bm()


id = IntruderDetector(16)
id.run()
