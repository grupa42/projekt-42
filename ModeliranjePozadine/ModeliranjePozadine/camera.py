﻿import cv2
import io
import numpy as np

class Camera:
    _cap = cv2.VideoCapture(0)
    #flag = cv2.CAP_PROP_OPENNI_MAX_BUFFER_SIZE if cv2.__version__ ==
    #_cap.set(flag, 1)

    @staticmethod
    def get_image():
        #for i in range(20):
        #    Camera._cap.grab()
        _, img = Camera._cap.read()
        return img