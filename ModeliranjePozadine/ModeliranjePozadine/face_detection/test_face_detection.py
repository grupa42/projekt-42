__author__ = 'Win7'
import numpy as np
import cv2
import sys


def detect_face(frame):
    faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(15, 15),
            flags=cv2.CASCADE_SCALE_IMAGE
    )

    if (len(faces) == 0):
        return None
    else:

        # Draw a rectangle around the faces
        for (x, y, w, h) in faces:
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

        # Display the resulting frame
        cv2.imwrite('detekcija.jpg', frame)
        return frame


def main():
    cap = cv2.VideoCapture(0)

    while (True):
        ret, frame = cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        cv2.imshow('video', gray)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            img = detect_face(frame)
            break

    cv2.destroyWindow('video')
    cv2.waitKey(0)

    cap.release()
    cv2.destroyAllWindows()
    return


main()
