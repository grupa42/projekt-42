__author__ = 'Win7'
import numpy as np
import cv2
import sys

class face_detection():

    @staticmethod
    def detect_face(frame):
        faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(5, 5),
            flags=cv2.CASCADE_SCALE_IMAGE
        )

        if(len(faces)==0):
            return None
        else:

            # Draw a rectangle around the faces
            for (x, y, w, h) in faces:
                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

            # Display the resulting frame
            #cv2.imshow('Video', frame)
            cv2.imwrite('detekcija.jpg',frame)
            return frame

