#!/usr/bin/env python

import cv2
import numpy as np

cv2.imshow('ekanje uljeza', np.zeros([1, 320]))
res = cv2.waitKey(0)
print 'You pressed %d (0x%x), 2LSB: %d (%s)' % (res, res, res % 2**16,
    repr(chr(res%256)) if res%256 < 128 else '?')