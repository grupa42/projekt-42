from background_model_old import *

# Testiranje

a = BackgroundModel((4, 4))
b = [Matrix((4, 4), np.uint8) for i in range(4)]
c = cv2.ACCESS_MASK

for i in range(4):
    for j in range(4):
        b[0][i, j] = i
        b[1][i, j] = j
        b[2][i, j] = i if i < j else j
        b[3][i, j] = 5 if i <= j else 6

a.create_from_images(b)
print(b[0].pixels)
print(b[1].pixels)
print(b[2].pixels)
print(b[3].pixels)
print('\n')
print(a.background.pixels)
print(a.number_of_samples)
plt.stem(a.pixel_value_distributions[2, 2], )
x1, x2, y1, y2 = plt.axis()
plt.axis((x1, 10, y1, y2))
plt.ylabel('BackgroundModel._histograms[2,2]')
plt.show()
