import numpy as np
from background_model import *


class GaussBackgroundModel(BackgroundModel):
    def __init__(self, imageSize, threshold=0, buffer_size=0):
        self.images = None
        self.imageSize = imageSize
        self.variance = None
        self.mean = None
        # self.k = k
        # self.threshold = threshold
        self._normalDistributions = None
        BackgroundModel.__init__(self, imageSize, threshold, buffer_size)

    def update(self, images=None):
        if images is None:
            images = np.array([img for img in self._buffer])
        self.images = images
        self._normalDistributions = self._gaussMixtureWithEM(2)

    def detect_foreground(self, image, denoise=False):
        var = self._normalDistributions[0]
        mu = self._normalDistributions[1]
        G = np.zeros(shape=mu.shape, dtype=np.uint8)
        G[np.abs(image - mu) / var ** 0.5 > 1.28155] = 255
        return self._filter_foreground_noise(G) if denoise else G

    def _resetData(self):
        self.data = np.zeros((self.imageSize[0], self.imageSize[1], len(self.images)), dtype=np.uint8)

    def _gaussMixtureWithEM(self, k):
        self._resetData()
        self._background = np.empty(self.imageSize, dtype=np.uint8)

        if k == 1:
            self.images = np.array(self.images)
            self.mean = self.images.mean(axis=0)
            self.variance = self.images.var(axis=0)

            normal = np.array([self.variance, self.mean])
            return normal

        counter = 0
        for img in self.images:

            for i in range(self.background.shape[0]):
                for j in range(self.background.shape[1]):
                    index = (i, j, counter)
                    self.data.itemset(index, img.item((i, j)))

            counter = counter + 1

        parametersList = np.zeros((self.background.shape[0], self.background.shape[1], 2, 3))
        parameters = np.zeros((2, 3))

        for i in range(self.background.shape[0]):
            for j in range(self.background.shape[1]):
                X = self.data[i, j, :]

                pi = [0.9, 0.1]
                mu = [X.mean(axis=0), 0.0]
                var = [X.var(axis=0), 1000.0]

                logLikelihoods = []

                if X.var(axis=0) != 0:

                    while (1):

                        responsibilities = np.zeros((self.images.shape[0], 2))

                        temp1 = temp2 = 0.0
                        for a in range(self.images.shape[0]):
                            temp1 += pi[0] / (2 * np.pi * var[0] ** 0.5) * np.exp(
                                -((X[a] - mu[0]) ** 2) / (2 * var[0]))
                            temp2 += pi[1] / (2 * np.pi * var[1] ** 0.5) * np.exp(
                                -((X[a] - mu[1]) ** 2) / (2 * var[1]))
                            temp = temp1 + temp2
                            responsibilities[a][0] = temp1 / temp
                            responsibilities[a][1] = temp2 / temp

                        temporarySum11 = temporarySum12 = temporarySum21 = temporarySum22 = 0.0
                        for a in range(self.images.shape[0]):
                            temporarySum11 += responsibilities[a][0] * X[a]
                            temporarySum12 += responsibilities[a][0]
                            temporarySum21 += responsibilities[a][1] * X[a]
                            temporarySum22 += responsibilities[a][1]

                        delta1 = mu[0]
                        delta2 = mu[1]
                        delta3 = var[0]
                        delta4 = var[1]
                        delta5 = pi[0]
                        delta6 = pi[1]

                        mu[0] = temporarySum11 / (temporarySum12 + 0.00001)
                        mu[1] = temporarySum21 / (temporarySum22 + 0.00001)

                        delta1 = delta1 - mu[0]
                        delta2 = delta2 - mu[1]

                        temporarySum11 = temporarySum12 = temporarySum21 = temporarySum22 = 0.0
                        for a in range(self.images.shape[0]):
                            temporarySum11 += responsibilities[a][0] * (X[a] - mu[0]) ** 2
                            temporarySum12 += responsibilities[a][0]
                            temporarySum21 += responsibilities[a][1] * (X[a] - mu[1]) ** 2
                            temporarySum22 += responsibilities[a][1]

                        var[0] = (temporarySum11 + 0.00001) / (temporarySum12 + 0.00001)
                        var[1] = (temporarySum21 + 0.00001) / (temporarySum22 + 0.00001)

                        pi[0] = temporarySum12 / self.images.shape[0]
                        pi[1] = temporarySum22 / self.images.shape[0]

                        delta3 = delta3 - var[0]
                        delta4 = delta4 - var[1]
                        delta5 = delta5 - pi[0]
                        delta6 = delta6 - pi[1]

                        logLikelihood = 0.0
                        for a in range(self.images.shape[0]):
                            temp1 = pi[0] * (1. / (2 * np.pi * var[0] ** 0.5) * np.exp(
                                -((X[a] - mu[0]) ** 2) / (2 * var[0])))
                            temp2 = pi[1] * (1. / (2 * np.pi * var[1] ** 0.5) * np.exp(
                                -((X[a] - mu[1]) ** 2) / (2 * mu[1])))
                            temp = temp1 + temp2
                            logLikelihood += np.log(temp)

                        logLikelihoods.append(logLikelihood)

                        if len(logLikelihoods) < 2: continue

                        if (pi[0] > pi[1]):
                            parameters[0][0] = pi[0]
                            parameters[0][1] = var[0] ** 0.5
                            parameters[0][2] = mu[0]
                            parameters[1][0] = pi[1]
                            parameters[1][1] = var[1] ** 0.5
                            parameters[1][2] = mu[1]
                        else:
                            parameters[0][0] = pi[1]
                            parameters[0][1] = var[1] ** 0.5
                            parameters[0][2] = mu[1]
                            parameters[1][0] = pi[0]
                            parameters[1][1] = var[0] ** 0.5
                            parameters[1][2] = mu[0]

                        if (np.abs(logLikelihoods[-1] - logLikelihoods[-2]) < 0.01 or
                                                        delta1 < 0.01 and delta2 < 0.01 and delta3 < 0.01 and
                                                delta4 < 0.01 and delta5 < 0.01 and delta6 < 0.01 or
                                    len(logLikelihoods) > 5): break

                else:
                    parameters[0][0] = 1.0
                    parameters[0][1] = X.var(axis=0)
                    parameters[0][2] = X.mean(axis=0)
                    parameters[1][0] = 0
                    parameters[1][1] = 0
                    parameters[1][2] = 0

                parametersList[i][j] = parameters.copy()
            print (i, j)

        self.mean = np.zeros(self.imageSize)
        self.variance = np.zeros(self.imageSize)

        for i in range(self.imageSize[0]):
            for j in range(self.imageSize[1]):
                self.mean[i][j] = parametersList[i][j][0][1]
                if parametersList[i][j][0][2] == 0:
                    self.variance[i][j] = parametersList[i][j][0][2] + 0.00001
                else:
                    self.variance[i][j] = parametersList[i][j][0][2]

        normal = np.array([self.variance, self.mean])
        return normal

'''
        mu = np.empty((self.imageSize[0], self.imageSize[1], self.k))
        var = np.empty((self.imageSize[0], self.imageSize[1], self.k))
        pi = np.zeros((self.imageSize[0], self.imageSize[1], self.k))
        pi[:, :, 0] = 1.0
        mu[:, :, :] = random.random() * 255
        var[:, :, :] = random.random() * 10000

        logLikelihoods = []

        while(1):

            U = norm.pdf(np.vstack([self.images, self.images]).T, loc = mu, scale = var**0.5)

            D = np.reshape(np.sum(pii *  U, axis = 1), (self.imagesSize[0], 1))

            H =  U / D

            DD = np.sum(H * np.reshape(X, (self.imagesSize[0], 1)), axis = 0)
            mu2 = np.sum(H, axis=0) / DD
            var2 = np.sum((np.vstack([X, X]).T - H)**2 * H, axis=0) / DD
            pi2 = DD / self.imagesSize[0]

            delta1 = np.max(np.abs(mu - mu2))
            delta2 = np.max(np.abs(var - var2))
            delta3 = np.max(np.array(pi - pi2))

            logLikelihood = norm.pdf(np.vstack([X, X]).T, loc = mu2, scale = var2**0.5)
            logLikelihoods.append(np.sum(logLikelihood))
'''
