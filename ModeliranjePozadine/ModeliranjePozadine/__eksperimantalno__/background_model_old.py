﻿import numpy as np
import matplotlib.pyplot as plt
import cv2

class Color:
    def __init__(self, r, g, b):
        self.r = r
        self.g = g
        self.b = b

    def to_greyscale(self):
        return (self.r + self.g + self.b) / 3   #privremeno
class Matrix:
    def __init__(self, size, datatype):
        self.size = size
        self.pixels = np.zeros(size, dtype=datatype)

    def __setitem__(self, key, value):
        self.pixels[key] = value


class BackgroundModel:
    def __init__(self, size):
        """:type size: tuple"""
        self.size = size
        self.background = Matrix(size, np.uint8)
        histogram_size = (size[0], size[1], 256)
        self.pixel_value_distributions = np.zeros(histogram_size, dtype=np.uint32)
        self.number_of_samples = 0

    def create_from_images(self, grey_bitmaps):
        """:type grey_bitmaps: list<Matrix>"""
        for gbmp in grey_bitmaps:
            for i in range(self.background.size[0]):
                for j in range(self.background.size[0]):
                    self.pixel_value_distributions[i, j, gbmp.pixels[i, j]] += 1
        for i in range(self.background.size[0]):
            for j in range(self.background.size[0]):
                self.background[i, j] = np.argmax(self.pixel_value_distributions[i, j])
        self.number_of_samples = len(grey_bitmaps)
