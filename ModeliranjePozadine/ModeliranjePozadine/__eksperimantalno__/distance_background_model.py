﻿# coding=utf-8
from background_model import *


class DistanceBackgroundModel(BackgroundModel):
    # Inicijalizira i odredjuje maksimalni broj slika koje objekt moze drzati
    def __init__(self, image_size, threshold=0.2, buffer_size=0):
        BackgroundModel.__init__(self, image_size, threshold, buffer_size)

    # Prepoznaje objekte prednjeg plana
    def detect_foreground(self, image, denoise=False):
        foreground_image = np.zeros(self.image_size, np.uint8)
        foreground_image[~np.isclose(image, self._background, atol=self._threshold * 255)] = 255
        return self._filter_foreground_noise(foreground_image) if denoise else foreground_image

    # Izradjuje model pozadine na temelju zadnjih n dodanih slika
    def update(self, images=None):
        if images is None:
            images = np.array([img for img in self._buffer])
        self._create_histograms(images)
        np.argmax(self._histograms, axis=2, out=self._background)

    def save_model(self, path):
        data = np.asarray((self.image_size, self._background, self._histograms, self._threshold))
        np.ndarray.dump(data, open(path, 'wb'))

    def load_model(self, path):
        self.image_size, self._background, self._histograms, self._threshold = np.load(open(path, 'rb'))
