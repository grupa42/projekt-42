import cv2
import numpy as np


def posivi(img):
    return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

def show_webcam(mirror=False):
    cam = cv2.VideoCapture(0)
    ret_val, img1 = cam.read()
    img1 = posivi(img1)

    ofi = np.zeros(img1.shape, np.uint8)
    while True:
        ret_val, img = cam.read()
        img = posivi(img)
        if mirror:
            img = cv2.flip(img, 1)

        #a = dbwizev(img, img1)
        foreground_image = np.zeros(img.shape, np.uint8)
        foreground_image[~np.isclose(img, img1, atol=5)] = 255
        cv2.imshow(type([1, 2, 3]).__name__, foreground_image)
        if cv2.waitKey(1) == 27:
            break  # esc to quit
        img1 = img

    cv2.destroyAllWindows()


def main():
    show_webcam(mirror=True)


main()
