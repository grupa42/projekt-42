﻿# coding=utf-8
import numpy as np
from limited_set import LimitedSet
from push_out_queue import PushOutQueue
from abc import ABCMeta
import cv2
import pickle


# Model pozadine
class BackgroundModel:
    __metaclass__ = ABCMeta

    # Inicijalizira i odredjuje maksimalni broj slika koje objekt moze drzati
    def __init__(self, image_size, threshold=0.2, buffer_size=0):
        """
        :param image_size: dimenzije pozadine (širina, visina)
        :param threshold: iz [0, 1] prag za razlikovanje prednjeg plana od pozadine
        :param buffer_size: broj slika koje se koriste kod dinamičnog modela pozadine
        """
        self.image_size = image_size  # dimenzije pozadine: (visina, širina)
        self._background = np.empty(self.image_size, dtype=np.uint8)  # pozadina
        self._threshold = threshold
        self._buffer = PushOutQueue(buffer_size)
        self._newbuff = PushOutQueue(buffer_size)
        self._histograms = np.zeros((self.image_size[0], self.image_size[1], 256), dtype=np.uint16)

    @property
    def background(self):
        return self._background

    @property
    def threshold(self):
        return self._threshold

    @threshold.setter
    def threshold(self, threshold):
        self._threshold = threshold

    # Dodaje sliku u skup slika za modeliranje pozadine, dinamički model
    def push_sample(self, image):
        self._buffer.push(image)

    def push_sample_update(self, image):
        subs = self._newbuff.push(image)
        if subs is not None:
            self._subtract_from_histograms(subs)
        self._add_to_histograms(image)
        np.argmax(self._histograms, axis=2, out=self._background)

    # Izradjuje model pozadine
    def update(self, images=None):
        raise NotImplemented

    def _create_histograms(self, images):  # statički model
        self._histograms = np.zeros((self.image_size[0], self.image_size[1], 256), dtype=np.uint16)
        for i in range(self.image_size[0]):
            for j in range(self.image_size[1]):
                self._histograms[i, j] = np.bincount(images[:, i, j], minlength=256)

    def _add_to_histograms(self, image):  # dinamički model
        for i in range(self.image_size[0]):
            for j in range(self.image_size[1]):
                ind = (i, j, image[i, j])
                self._histograms.itemset(ind, self._histograms.item(ind) + 1)
                # self._histograms[i, j, image[i, j]] += 1

    def _subtract_from_histograms(self, image):  # dinamički model
        for i in range(self.image_size[0]):
            for j in range(self.image_size[1]):
                ind = (i, j, image[i, j])
                self._histograms.itemset(ind, self._histograms.item(ind)-1)

    def _smooth_histograms(self, n_times, factor):
        factor /= 2
        old_histograms = np.copy(self._histograms)
        for i in range(self._histograms.shape[0]):
            for j in range(self._histograms.shape[1]):
                for k in range(255):
                    change = int(factor * (old_histograms.item(i, j, k + 1) - old_histograms.item(i, j, k)))
                    np.add.at(self._histograms, (i, j, k), change)
                    np.add.at(self._histograms, (i, j, k + 1), -change)
            print(repr(i) + "/" + repr(self._histograms.shape[0]))

    # Prepoznaje objekte prednjeg plana
    def detect_foreground(self, image, denoise=False):
        raise NotImplemented

    # uzima najčešću boju u okruženju svakog piksela
    @staticmethod
    def _filter_foreground_noise(fg):
        return cv2.medianBlur(fg, 5)

    def save_model(self, path):
        raise NotImplemented

    def load_model(self, path):
        raise NotImplemented
